package calculator
package basic

class BasicEvaluator extends Evaluator[BasicExpr, BasicEvaluator.Result]:
  import BasicExpr.*
  import BasicEvaluator.*
  import BasicEvaluator.Result.*

  /** Evaluate an expression to its value. */
  def evaluate(e: BasicExpr): Result =
    Ok(42) // TODO: modify this

object BasicEvaluator:
  enum Result:
    case Ok(v: Double)
    case DivByZero

    def get: Double = this match
      case Ok(v)     => v
      case DivByZero => throw new RuntimeException(s"division by zero")
