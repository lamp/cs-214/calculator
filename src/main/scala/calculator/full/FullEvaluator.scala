package calculator
package full

import scala.util.{Try, Success, Failure}

object FullEvaluator:
  /** Result of evaluation. */
  enum Result:
    case Ok(v: Double)
    case DivByZero
    case UndefinedVar(name: String)

    def get: Double = this match
      case Ok(v)              => v
      case DivByZero          => throw new RuntimeException("division by zero")
      case UndefinedVar(name) => throw new RuntimeException(s"undefined variable: $name")

  // Define your own context here

  type Context =
    Unit // TODO: modify this

  object Context:
    def empty: Context =
      () // TODO: modify this

    def cons(name: String, value: Double, tail: Context) =
      () // TODO: modify this

    def fromList(xs: List[(String, Double)]): Context =
      xs match
        case Nil           => empty
        case (n, v) :: rem => cons(n, v, fromList(rem))

class FullEvaluator(ctx: FullEvaluator.Context) extends Evaluator[FullExpr, FullEvaluator.Result]:
  import FullEvaluator.*
  import FullExpr.*
  import Result.*

  /** Evaluate an expression to its value. */
  def evaluate(e: FullExpr): Result =
    Ok(42) // TODO: modify this
