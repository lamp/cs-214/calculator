package calculator
package full

/** Simplify expressions based on the listed algebraic rules
  * {{{
  * 1. 0 + e = e + 0 = e
  * 2. 0 - e = -e
  * 3. e - 0 = e
  * 4. 0 * e = e * 0 = 0
  * 5. 1 * e = e * 1 = e
  * 6. e / 1 = e
  * 7. e - e = 0
  * 8. -(-e) = e
  * }}}
  */
object AlgebraicSimplifier extends Simplifier[FullExpr]:
  import FullExpr.*

  def simplify(e: FullExpr): FullExpr =
    e // TODO: modify this
