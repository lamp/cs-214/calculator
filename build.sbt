name := "calculator"
scalaVersion := "3.5.0"
scalacOptions ++= Seq("-deprecation", "-feature", "-language:fewerBraces", "-Xfatal-warnings")
run / fork := true
run / connectInput := true
run / outputStrategy := Some(StdoutOutput)
Global / cancelable := true

val caskVersion = "0.9.4"
val munitVersion = "1.0.1"
libraryDependencies += "com.lihaoyi" %% "cask" % caskVersion
libraryDependencies += "org.scalameta" %% "munit" % munitVersion % Test
